<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ApiModel extends Model
{
    
   function getAllOrder($id){
    return $orders = DB::table('orders')
    ->select('orders.orderNumber as order_id','orders.orderDate as order_date','orders.status')
    ->join('customers', 'customers.customerNumber', '=', 'orders.customerNumber')
    ->where('orders.orderNumber', '=', $id)
    ->get();
   }

   function getAllOrderDetail($id){
    return $orders = DB::table('orderdetails')
    ->select('products.productName as product','products.productLine as product_line','orderdetails.priceEach as unit_price','orderdetails.quantityOrdered as qty','orderdetails.orderLineNumber as line_total')
    ->join('products', 'products.productCode', '=', 'orderdetails.productCode')
    ->where('orderdetails.orderNumber', '=', $id)
    ->get();

   }

   function customerDetail($id){
    return $orders = DB::table('orders')
    ->select('customers.contactFirstName as first_name','customers.contactLastName as last_name','customers.phone','customers.country as country_code')
    ->join('customers', 'customers.customerNumber', '=', 'orders.customerNumber')
    ->where('orders.orderNumber', '=', $id)
    ->get();

   }
}
