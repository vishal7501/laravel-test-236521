<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\ApiModel;
class OrdersController extends Controller
{
    public function __construct(){
    	$this->ApiModel = new ApiModel();
    }


    /**
     * fetch order details
     * @param $id
     * @return array
     */
    public function fetchOrderData($id)
    {
        //echo $id; exit;
        $data['getAllorder'] = $this->ApiModel->getAllOrder($id);
        $responceArr['order'] = $data['getAllorder'][0];
        $responceArr['order_details'] = $this->ApiModel->getAllOrderDetail($id);
        $total_amt = [];
        foreach($responceArr['order_details'] as $row){
            $total_amt[] = ($row->unit_price*$row->qty);
        }
        $new_total_amt = array_sum($total_amt);
        $responceArr['bill_amount'] = number_format($new_total_amt,2);
        $responceArr['customer'] = $this->ApiModel->customerDetail($id)[0];
        if(empty($responceArr['order'])){
            $responceArr = [];
            return response()->json($responceArr,401);
        }else{
            return response()->json($responceArr,200);
            

        }
        
    }
}
